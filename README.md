# css-name-encrypter

> CSS 클래스 명을 encryption 하는 모듈이다.

## encrypt
`CSSNameEncrypter` 는 클래스 명을 다음의 순서로 변환한다.
1. Lowercase
2. md5
3. bs64
4. bs58
5. Character substring

- base58 로 변환하는 이유는 *, +, /, = 특수문자를 제거하기 위함이다.
- 마지막에 character substring 작업을 하는데, 3글자에서 시작하고, 변환된 정보는 저장이 된다. 중복이 될 경우 한글자씩 늘려나간다.

## How to use
```js
const CSSNameEncrypter = require('css-name-encrypter')
const cssEncrypter = new CSSNameEncrypter()

postcss([
  require('postcss-modules')({
    generateScopedName: function(name, filename, css) {
      return cssEncrypter.encrypt(name)
    }
  })
])
```

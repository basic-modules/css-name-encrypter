const crypto = require('crypto')
const bs58 = require('bs58')

class CSSNameEncrypter {
  constructor() {
    this.cssModuleEncryptList = []
    this.cssModuleEncryptObject = {}
    this.cssClasses = {}
  }

  encrypt(name) {
    let localName = name.toLowerCase()
    let charCount = 3
    let outputPrefix = '_'

    // 이름을 md5로 해쉬화 시킨 후 base64로 변환 후 base58로 변환(특수문자 제거)
    let _base64 = crypto
      .createHash('md5')
      .update(localName)
      .digest('base64')
    let encryptedName = bs58.encode(new Buffer(_base64))
    encryptedName = encryptedName.toLowerCase()

    // 초기 이름 설정. 처음에는 3글자로 자름
    let outputEncryptedName = encryptedName.substr(0, charCount)

    // 현재 이름에 해당하는 값이 없을 경우 그대로 반환
    if (this.cssModuleEncryptObject[localName] != null) {
      this.cssModuleEncryptList.push(outputEncryptedName)
      return outputPrefix + this.cssModuleEncryptObject[localName]
    }

    // 이름이 존재할 경우, 한글자씩 길이를 늘려감.
    while (this.cssModuleEncryptList.indexOf(outputEncryptedName) > -1) {
      charCount++
      outputEncryptedName = encryptedName.substr(0, charCount)
    }

    var output = outputPrefix + outputEncryptedName
    this.cssModuleEncryptList.push(outputEncryptedName)
    this.cssModuleEncryptObject[localName] = outputEncryptedName
    this.cssClasses[localName] = output

    return output
  }

  getName(name) {
    return this.cssClasses[name.toLowerCase()]
  }

  toJSON () {
    return this.cssClasses
  }
}

module.exports = CSSNameEncrypter
